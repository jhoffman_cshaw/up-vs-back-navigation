package ca.jhoffman.handleupandback;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by jhoffman on 2016-10-05.
 */

public class ValidateUpAndBackFragmentDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Go back???")
                .setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((onValidateUpAndBackListener)getActivity()).onValidate(true);
                    }
                })
                .setNegativeButton("No...",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((onValidateUpAndBackListener)getActivity()).onValidate(false);
                    }
                });

        return builder.create();
    }

    public interface onValidateUpAndBackListener {
        void onValidate(boolean goBack);
    }
}
