package ca.jhoffman.handleupandback;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity implements ValidateUpAndBackFragmentDialog.onValidateUpAndBackListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    @Override
    public void onBackPressed() {
        validate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            validate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void validate() {
        new ValidateUpAndBackFragmentDialog().show(getSupportFragmentManager(), "ValidateUpAndBackFragmentDialog");
    }

    //
    // onValidateUpAndBackListener
    //

    @Override
    public void onValidate(boolean goBack) {
        if (goBack == true) {
            finish();
        } else {
            Toast.makeText(this, "Not going back....", Toast.LENGTH_SHORT).show();
        }
    }
}
